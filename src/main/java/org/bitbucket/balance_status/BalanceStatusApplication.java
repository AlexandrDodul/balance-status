package org.bitbucket.balance_status;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class BalanceStatusApplication {

    public static void main(String[] args) {
        SpringApplication.run(BalanceStatusApplication.class, args);

    }

}
